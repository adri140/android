package com.songs.mysongsapp.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.songs.mysongsapp.AddSongsActivity;
import com.songs.mysongsapp.MainActivity;
import com.songs.mysongsapp.PopUpSongsActivity;
import com.songs.mysongsapp.R;
import com.songs.mysongsapp.adaptadores.*;
import com.songs.mysongsapp.clases.Song;

import java.util.List;

public class FragmentSongs extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener, AdapterView.OnItemLongClickListener
{
    private List<Song> songs;
    private Activity padre;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        padre = getActivity();

        View view;
        view = inflater.inflate(R.layout.fragment_songs, container, false);

        ListView list = view.findViewById(R.id.songs);

        this.songs = Song.recoverSongs(getActivity());

        ArrayAdapter adapter = new SongAdaptador(getActivity(), songs);
        list.setAdapter(adapter);

        list.setOnItemClickListener(this);
        list.setOnItemLongClickListener(this);

        Button addSong = view.findViewById(R.id.addSong);
        addSong.setOnClickListener(this);

        return view;

        //return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
       ((MainActivity) getActivity()).sendSongToMediaPlayer(this.songs.get(position));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.addSong:
                Intent search = new Intent(getActivity(), AddSongsActivity.class);
                startActivityForResult(search, 32);
                break;
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
    {
        Intent intent = new Intent(getActivity(), PopUpSongsActivity.class);
        intent.putExtra("song", songs.get(position).getId());
        startActivityForResult(intent, 30);
        return true;
    }
}
