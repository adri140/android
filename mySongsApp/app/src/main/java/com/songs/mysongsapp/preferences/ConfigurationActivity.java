package com.songs.mysongsapp.preferences;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.songs.mysongsapp.R;

public class ConfigurationActivity extends PreferenceActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference_options);
    }
}
