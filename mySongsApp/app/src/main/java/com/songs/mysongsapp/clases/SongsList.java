package com.songs.mysongsapp.clases;

import android.content.ContentValues;
import android.content.Context;

import com.songs.mysongsapp.database.Dao;
import com.songs.mysongsapp.database.RelationalDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SongsList
{
    private Integer id;
    private List<Song> list;
    private String listName;
    private static Dao dao;
    private static RelationalDao daoRelacion;

    private static void prepareDao(Context context)
    {
        if(dao == null) dao = new Dao("songslist", "id" , context);
        if(daoRelacion == null) daoRelacion = new RelationalDao("songs_as_songslist", context);
    }

    public static SongsList createObject(String listName, Context context)
    {
        SongsList list = new SongsList(listName);
        prepareDao(context);

        System.out.println(dao.getTableName());
        list.setId(dao.insertObject(list.getValues()));

        return list;
    }

    public static SongsList recoverSongsList(Integer id, Context context)
    {
        SongsList list = null;
        prepareDao(context);
        ContentValues[] values = dao.getValues(id + "");
        if (values != null) list = values[0] != null ? new SongsList(values[0], context) : null;

        return list;
    }

    public static List<SongsList> recoverSongsLists(Context context)
    {
        List<SongsList> lists = new ArrayList<SongsList>();
        prepareDao(context);
        ContentValues[] values = dao.getValues();

        if (values != null) {
            for (ContentValues value : values) {
                lists.add(new SongsList(value, context));
            }
        }
        return lists;
    }

    private ContentValues getValues()
    {
        ContentValues values = new ContentValues();
        values.put("listName", this.listName);
        return values;
    }

    public boolean deleteThisSongsList(Context context)
    {
        prepareDao(context);

        return daoRelacion.deleteObject("idSongsList", this.getId() + "") ? dao.deleteObject(this.id + "") : false;
    }

    public boolean updateThisSongsList(Context context)
    {
        prepareDao(context);
        return dao.updateObject(this.getValues(), this.id + "") == 1 ? true : false;
    }

    private SongsList(String listName)
    {
        this.listName = listName;
        this.list = new ArrayList<Song>();
    }

    private SongsList(ContentValues values, Context context)
    {
        this.id = values.getAsInteger("id");
        this.listName = values.getAsString("listName");
        this.list = new ArrayList<Song>();
    }

    public List<Song> getSongs(Context context)
    {
        ContentValues[] values = daoRelacion.getValues("idSongsList", this.id + "");

        if(list == null) list = new ArrayList<>();

        if(values != null) {
            for (ContentValues value : values) {
                list.add(Song.recoverSong(Integer.parseInt(value.getAsString("idSongs")), context));
            }
        }

        return list;
    }

    private void setId(Integer id)
    {
        this.id = id;
    }

    public void setListName(String listName)
    {
        this.listName = listName;
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getListName()
    {
        return this.listName;
    }

    public void addSongs(Context context, Song...songs)
    {
        if(list == null) list = new ArrayList<>();

        prepareDao(context);
        for(Song song : songs)
        {
            ContentValues values = new ContentValues();
            values.put("idSongs", song.getId());
            values.put("idSongsList", this.getId());

            daoRelacion.insertObject(values);
            list.add(song);
        }
    }

    public void quitarCancion(Song song, Context context)
    {
        list = this.getSongs(context);

        if(list.size() > 0 && list.contains(song)) {

            Map<String, String> params = new HashMap<>();
            params.put("idSongsList", this.getId() + "");
            params.put("idSongs", song.getId() + "");

            daoRelacion.deleteObject(params);
        }
    }
}
