package com.songs.mysongsapp.adaptadores;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.songs.mysongsapp.R;
import com.songs.mysongsapp.clases.SongsList;

import java.util.ArrayList;
import java.util.List;

public class ListSongAdapter extends ArrayAdapter
{
    private List<SongsList> listas;
    private Activity context;

    public ListSongAdapter(Activity context, List<SongsList> listas) {
        super(context, R.layout.items_list_lists, listas);

        this.listas = listas == null ? new ArrayList<SongsList>() : listas;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.items_list_lists, parent, false);
        }

        TextView title = convertView.findViewById(R.id.titleList);

        title.setText(listas.get(position).getListName());

        return convertView;
        //return super.getView(position, convertView, parent);
    }
}
