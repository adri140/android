package com.songs.mysongsapp.adaptadores;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.songs.mysongsapp.R;
import com.songs.mysongsapp.clases.Song;

import java.util.ArrayList;
import java.util.List;

public class SongAdaptador extends ArrayAdapter
{
    private List<Song> songs;
    private Activity context;

    public SongAdaptador(Activity context, List<Song> songs)
    {
        super(context, R.layout.items_list_songs, songs);
        this.songs = songs == null? new ArrayList<Song>() : songs;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        /*LayoutInflater inflater = context.getLayoutInflater();

        // Sobre el layout crear (inflat) dupliquem el layour creat amb els objectes, view personals.
        View item = inflater.inflate(R.layout.items_list_songs, null);*/

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.items_list_songs, parent, false);
        }

        TextView title = convertView.findViewById(R.id.titleSong);

        title.setText(songs.get(position).getSongName());

        return convertView;
    }
}
