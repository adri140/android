package com.songs.mysongsapp.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.songs.mysongsapp.R;
import com.songs.mysongsapp.clases.Song;
import com.songs.mysongsapp.clases.SongsList;
import com.songs.mysongsapp.tmpClass.MyMediaPlayer;

public class FragmentMediaPlayer extends Fragment implements View.OnClickListener
{
    private static MyMediaPlayer player;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view;
        view = inflater.inflate(R.layout.fragment_mediaplayer, container, false);

        prepareButtons(view);
        preparePlayer();

        Button ini = view.findViewById(R.id.iniciar);
        player.setIniciar(ini);

        if(player.getPlay()) ini.setBackgroundResource(android.R.drawable.ic_media_pause);
        else ini.setBackgroundResource(android.R.drawable.ic_media_play);

        return view;
    }

    private void prepareButtons(View view)
    {
        Button iniciar = view.findViewById(R.id.iniciar);
        iniciar.setOnClickListener(this);

        Button siguiente = view.findViewById(R.id.siguiente);
        siguiente.setOnClickListener(this);

        Button anterior = view.findViewById(R.id.anterior);
        anterior.setOnClickListener(this);

        Button sound = view.findViewById(R.id.soundButton);
        sound.setOnClickListener(this);

        if(player == null) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            //SharedPreferences.Editor editorpreferencies = prefs.edit();
            boolean song = prefs.getBoolean("songOptionPreference", true);

            if (!song) sound.setBackgroundResource(android.R.drawable.ic_lock_silent_mode);
            else sound.setBackgroundResource(android.R.drawable.ic_lock_silent_mode_off);
        }
        else
        {
            if(!player.getVolumen()) sound.setBackgroundResource(android.R.drawable.ic_lock_silent_mode);
            else sound.setBackgroundResource(android.R.drawable.ic_lock_silent_mode_off);
        }
    }

    //prepara el media player
    private void preparePlayer()
    {
        player = MyMediaPlayer.singelton(getActivity(), null);
    }

    //cambia la cancion
    public void changeSong(Song song)
    {
        preparePlayer();
        player.addNewSong(song);
    }

    //cambia la lista de canciones
    public void changeList(SongsList lista)
    {
        preparePlayer();
        player.addNewList(lista);
    }

    @Override
    public void onClick(View v) {
        Button iniciar = this.getView().findViewById(R.id.iniciar);
        switch (v.getId())
        {
            case R.id.iniciar:
                if(player.playSongs()) iniciar.setBackgroundResource(android.R.drawable.ic_media_pause);
                else iniciar.setBackgroundResource(android.R.drawable.ic_media_play);
                break;
            case R.id.anterior:
                if(player.onAnterior()) iniciar.setBackgroundResource(android.R.drawable.ic_media_pause);
                break;
            case R.id.siguiente:
                if(player.onSiguiente()) iniciar.setBackgroundResource(android.R.drawable.ic_media_pause);
                break;
            case R.id.soundButton:
                Button sound = this.getView().findViewById(R.id.soundButton);
                if(player.modifyVolumen()) sound.setBackgroundResource(android.R.drawable.ic_lock_silent_mode_off);
                else sound.setBackgroundResource(android.R.drawable.ic_lock_silent_mode);
                break;
        }
    }
}
