package com.songs.mysongsapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.Map;

public class RelationalDao {
    private DataBase database;
    private String tableName;

    public RelationalDao(String tableName, Context context)
    {
        this.database = DataBase.singelton(context);
        this.tableName = tableName;
    }

    public String getTableName(){
        return this.tableName;
    }

    //inserta en la base de datos
    public void insertObject(ContentValues values){
        SQLiteDatabase db  = database.getWritableDatabase();
        try{
            db.insertOrThrow(tableName, null, values); //devuelve el id
        }
        catch(SQLException e){
            Log.i("MyErrorDB", "InsertError: " + e.getMessage());
        }
        finally{
            db.close();
        }
    }

    //devuelve un content value de la tabla de la que quieres obtener datos
    public ContentValues[] getValues(String columnName, String...ids){
        ContentValues[] allValues = null;

        SQLiteDatabase db = database.getReadableDatabase();

        System.out.println("getValues");
        try{
            Cursor c_cursor = null;
            if(ids.length > 0) {
                String sql = "SELECT * FROM " + tableName + " WHERE " + columnName + " = ?";

                c_cursor = db.rawQuery(sql, ids);
            }
            else
            {
                System.out.println("recover all");
                c_cursor = db.rawQuery("SELECT * FROM " + tableName, ids);
            }

            if(c_cursor.moveToFirst()){
                allValues = new ContentValues[c_cursor.getCount()];
                int i = 0;
                do{
                    allValues[i] = new ContentValues();
                    for(Integer p = 0; p < c_cursor.getColumnCount(); p++){
                        String columnNameT = c_cursor.getColumnName(p);
                        //menuda mierda, no hay para extraer un Date, veras tu para sacar-lo de la base de datos
                        String value = c_cursor.getString(p);

                        allValues[i].put(columnNameT, value);
                    }
                    i++;
                }while(c_cursor.moveToNext());
            }
        }
        catch(SQLException e){
            Log.i("MyErrorDB", "Error: " + e.getMessage());
        }
        finally {
            db.close();
        }
        return allValues;
    }

    public boolean deleteObject(String columnName, String...ids)
    {
        boolean result = false;

        SQLiteDatabase db = database.getWritableDatabase();

        try
        {
            db.delete(this.tableName, columnName + " = ?", ids);
            result = true;
        }
        catch(SQLException e)
        {
            Log.i("MyErrorDB", "Error: " + e.getMessage());
        }
        finally
        {
            db.close();
        }
        return result;
    }

    public boolean deleteObject(Map<String, String> relationDelete)
    {
        boolean result = false;

        if(relationDelete.size() > 0) {

            String where = "";
            String[] args = new String[relationDelete.size()];

            int i = 0;
            for(String key : relationDelete.keySet())
            {
                where = where + key + " = ? AND ";
                args[i] = relationDelete.get(key);
                i++;
            }

            where = where.substring(0, where.lastIndexOf("AND"));

            Log.i("info", where);

            SQLiteDatabase db = database.getWritableDatabase();

            try {
                Integer deleted = db.delete(this.tableName, where, args);
                result = true;
            } catch (SQLException e) {
                Log.i("MyErrorDB", "Error: " + e.getMessage());
            } finally {
                db.close();
            }
        }
        return result;
    }
}
