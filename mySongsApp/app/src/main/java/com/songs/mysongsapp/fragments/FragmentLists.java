package com.songs.mysongsapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.songs.mysongsapp.AddListActivity;
import com.songs.mysongsapp.MainActivity;
import com.songs.mysongsapp.PopUpListActivity;
import com.songs.mysongsapp.R;
import com.songs.mysongsapp.adaptadores.ListSongAdapter;
import com.songs.mysongsapp.clases.Song;
import com.songs.mysongsapp.clases.SongsList;

import java.util.List;

public class FragmentLists extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener, AdapterView.OnItemLongClickListener
{

    private List<SongsList> lists;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view;
        view = inflater.inflate(R.layout.fragment_lists, container, false);

        lists = SongsList.recoverSongsLists(getActivity());
        ArrayAdapter adapter = new ListSongAdapter(getActivity(), lists);
        ListView list = view.findViewById(R.id.listas);
        list.setAdapter(adapter);

        list.setOnItemClickListener(this);
        list.setOnItemLongClickListener(this);

        Button addList = view.findViewById(R.id.addList);
        addList.setOnClickListener(this);

        return view;

        //return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        List<Song> songs = lists.get(position).getSongs(getActivity());

        if(songs.size() > 0) {
            ((MainActivity) getActivity()).sendSongsListToMediaPlayer(lists.get(position));
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.addList:
                Intent intent = new Intent(getActivity(), AddListActivity.class);
                startActivityForResult(intent, 34);
                break;
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), PopUpListActivity.class);
        intent.putExtra("listId", lists.get(position).getId());
        startActivityForResult(intent, 28);
        return false;
    }
}
