package com.songs.mysongsapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.songs.mysongsapp.clases.SongsList;

public class AddListActivity extends Activity implements View.OnClickListener
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_list);

        prepareButtons();

    }

    private void prepareButtons()
    {
        Button add = findViewById(R.id.addList);
        Button cancel = findViewById(R.id.cancelButton);

        add.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.addList:
                EditText edit = findViewById(R.id.listTitle);
                SongsList.createObject(edit.getText().toString(), this);
                break;
        }
        setResult(RESULT_OK);
        finish();
    }
}
