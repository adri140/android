package com.songs.mysongsapp.adaptadores;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.songs.mysongsapp.R;
import com.songs.mysongsapp.clases.Song;

import java.util.ArrayList;
import java.util.List;

public class ListListAdapter extends ArrayAdapter
{
    private List<Song> songs;
    private Activity context;

    public ListListAdapter(Activity context, List<Song> songs)
    {
        super(context, R.layout.items_songs_in_list, songs);
        this.songs = songs == null? new ArrayList<Song>() : songs;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        /*LayoutInflater inflater = context.getLayoutInflater();

        // Sobre el layout crear (inflat) dupliquem el layour creat amb els objectes, view personals.
        View item = inflater.inflate(R.layout.items_songs_in_list, null);*/

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.items_songs_in_list, parent, false);
        }

        TextView title = convertView.findViewById(R.id.songName);

        title.setText(songs.get(position).getSongName());

        return convertView;
    }
}
