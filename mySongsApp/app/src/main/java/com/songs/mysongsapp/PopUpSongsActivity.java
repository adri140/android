package com.songs.mysongsapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.songs.mysongsapp.clases.Song;
import com.songs.mysongsapp.clases.SongsList;

import java.util.ArrayList;
import java.util.List;

public class PopUpSongsActivity extends Activity implements View.OnClickListener
{
    private Spinner spinnerList;
    private List<SongsList> lists;
    private ArrayAdapter<String> adapter;
    private Song song;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_songs);

        Intent intent = getIntent();
        Integer id = (Integer) intent.getSerializableExtra("song");

        this.song = Song.recoverSong(id, this);

        System.out.println(song.getSongName());

        //preparamos el popUp
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width*.8), (int) (height*.3));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = -20;

        getWindow().setAttributes(params);
        //terminamos de preparar el popUp

        lists = SongsList.recoverSongsLists(this);
        spinnerList = findViewById(R.id.spinnerLists);

        prepareButtons();
        prepareSpinner();
    }

    private void prepareButtons()
    {
        Button addToList = findViewById(R.id.addToList);
        Button deleteThisSong = findViewById(R.id.deleteSong);

        addToList.setOnClickListener(this);
        deleteThisSong.setOnClickListener(this);
    }

    private void prepareSpinner()
    {
        List<String> items = new ArrayList<>();

        for(SongsList list : lists)
        {
            items.add(list.getListName());
        }

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, items);
        this.spinnerList.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.addToList:
                addToList();
                break;
            case R.id.deleteSong:
                delete();
                break;
        }
        setResult(RESULT_OK);
        finish();
    }

    private void addToList()
    {
        if(lists.size() > 0)
        {
            String selected = (String) spinnerList.getSelectedItem();

            for(SongsList list : lists)
            {
                if(list.getListName().equals(selected))
                {
                    if(song != null) list.addSongs(this, song);
                    break;
                }
            }
        }
    }

    private void delete()
    {
        if(song != null)
        {
            Log.i("DeleteSong", "Delete");
            song.deleteThisSong(this);
        }
    }
}
