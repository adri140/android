package com.songs.mysongsapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.songs.mysongsapp.adaptadores.ListListAdapter;
import com.songs.mysongsapp.adaptadores.ListSongAdapter;
import com.songs.mysongsapp.clases.Song;
import com.songs.mysongsapp.clases.SongsList;

import java.util.List;

public class PopUpListActivity extends Activity implements View.OnClickListener, AdapterView.OnItemLongClickListener
{

    private SongsList theList;
    private List<Song> allSongs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_listas);

        //preparamos el popUp
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width*.8), (int) (height*.5));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = -20;

        getWindow().setAttributes(params);
        //terminamos de preparar el popUp

        Intent data = getIntent();
        Integer list = ((Intent) data).getIntExtra("listId", -1);

        if(list == -1)
        {
            setResult(RESULT_OK);
            finish();
        }

        theList = SongsList.recoverSongsList(list, this);
        allSongs = theList.getSongs(this);

        if(allSongs.size() > 0) {

            ArrayAdapter adapter = new ListListAdapter(this, allSongs);

            ListView songs = findViewById(R.id.allSongs);
            songs.setOnItemLongClickListener(this);
            songs.setAdapter(adapter);
        }

        Button deleteButton = findViewById(R.id.deleteList);
        deleteButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.deleteList:
                delete();
                break;
        }
    }

    private void delete()
    {
        theList.deleteThisSongsList(this);
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        theList.quitarCancion(allSongs.get(position), this);
        setResult(RESULT_OK);
        finish();
        return false;
    }
}
