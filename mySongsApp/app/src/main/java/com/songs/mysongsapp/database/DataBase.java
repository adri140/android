package com.songs.mysongsapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBase extends SQLiteOpenHelper
{
    private static final String DATABASE_NAME = "songme";
    private static final int DATABASE_VERSION = 14;
    //VERSION 6, SE AÑADE EL ON DELETE CASCADE

    private static DataBase database;

    private static String tableSongs = "CREATE TABLE songs( " +
            "id Integer primary key AUTOINCREMENT, " +
            "songName Varchar(100) not null, " +
            "songUri Varchar(100) not null" +
            ");";
    private static String tableSongsLists = "CREATE TABLE songslist( " +
            "id Integer primary key AUTOINCREMENT, " +
            "listName Varchar(100) UNIQUE" +
            ");";
    private static String tableSongsAsSongsLists = "CREATE TABLE songs_as_songslist( " +
            "idSongs Integer, " +
            "idSongsList Integer, " +
            "primary key (idSongs, idSongsList), " +
            "foreign key (idSongs) references songs(id) ON DELETE CASCADE, " +
            "foreign key (idSongsList) references songslist(id) ON DELETE CASCADE " +
            ");";

    protected static DataBase singelton(Context context)
    {
        if(database == null)
            database = new DataBase(context);
        return database;
    }

    private DataBase(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //al crear la base de datos
        db.execSQL(tableSongs);
        db.execSQL(tableSongsLists);
        db.execSQL(tableSongsAsSongsLists);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //en caso de actualizacion de la base de datos
        if(oldVersion != newVersion) {

            db.execSQL("DROP TABLE IF EXISTS songs_as_songslist;");
            db.execSQL("DROP TABLE IF EXISTS songs;");
            db.execSQL("DROP TABLE IF EXISTS songslist;");


            this.onCreate(db);
        }
    }

    @Override
    public void onConfigure(SQLiteDatabase db)
    {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true); //habilitamos las foreign keys
    }
}
