package com.songs.mysongsapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Dao
{
    private DataBase database;
    private String tableName;
    private String idName;

    public Dao(String tableName, String idName, Context context)
    {
        this.database = DataBase.singelton(context);
        this.tableName = tableName;
        this.idName = idName;
    }

    public String getTableName(){
        return this.tableName;
    }

    //inserta en la base de datos
    public Integer insertObject(ContentValues values){
        Integer id = 0;
        SQLiteDatabase db  = database.getWritableDatabase();
        try{
            long l = db.insertOrThrow(tableName, null, values); //devuelve el id
            id = (int) (long) l;
        }
        catch(SQLException e){
            Log.i("MyErrorDB", "InsertError: " + e.getMessage());
        }
        finally{
            db.close();
        }
        return id;
    }

    //devuelve un content value de la tabla de la que quieres obtener datos
    public ContentValues[] getValues(String...ids){
        ContentValues[] allValues = null;

        SQLiteDatabase db = database.getReadableDatabase();

        System.out.println("getValues");
        try{
            Cursor c_cursor = null;
            if(ids.length > 0) {
                String sql = "SELECT * FROM " + tableName + " WHERE " + idName + " = ?";

                c_cursor = db.rawQuery(sql, ids);
            }
            else
            {
                System.out.println("recover all");
                c_cursor = db.rawQuery("SELECT * FROM " + tableName, ids);
            }

            if(c_cursor.moveToFirst()){
                allValues = new ContentValues[c_cursor.getCount()];
                int i = 0;
                do{
                    allValues[i] = new ContentValues();
                    for(Integer p = 0; p < c_cursor.getColumnCount(); p++){
                        String columnName = c_cursor.getColumnName(p);
                        //menuda mierda, no hay para extraer un Date, veras tu para sacar-lo de la base de datos
                        String value = c_cursor.getString(p);

                        allValues[i].put(columnName, value);
                    }
                    i++;
                }while(c_cursor.moveToNext());
            }
        }
        catch(SQLException e){
            Log.i("MyErrorDB", "Error: " + e.getMessage());
        }
        finally {
            db.close();
        }
        return allValues;
    }

    public boolean deleteObject(String...ids)
    {
        boolean result = false;

        SQLiteDatabase db = database.getWritableDatabase();

        try
        {
            Integer deleted = db.delete(this.tableName, this.idName + " = ?", ids);
            result = deleted > 0 ? true : false;
        }
        catch(SQLException e)
        {
            Log.i("MyErrorDB", "Error: " + e.getMessage());
        }
        finally
        {
            db.close();
        }

        return result;
    }

    public Integer updateObject(ContentValues values, String...id)
    {
        SQLiteDatabase db = database.getWritableDatabase();

        Integer updateadas = 0;

        try
        {
            updateadas = db.update(this.tableName, values, this.idName + " = ?", id);
        }
        catch(SQLException e)
        {
            Log.i("MyErrorDB", "Error: " + e.getMessage());
        }
        finally
        {
            db.close();
        }
        return updateadas;
    }

}
