package com.songs.mysongsapp.clases;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.songs.mysongsapp.database.Dao;
import com.songs.mysongsapp.database.RelationalDao;

import java.util.ArrayList;
import java.util.List;

public class Song
{
    private Integer id;
    private String songName;
    private Uri songUri;
    private static Dao dao;
    private static RelationalDao daoRelacional;

    //base de datos
    private static void prepareDao(Context context)
    {
        if(dao == null) dao = new Dao("songs", "id" , context);
        if(daoRelacional == null) daoRelacional = new RelationalDao("songs_as_songslist", context);
    }

    //inserta una cancion
    public static Song createSong(String songName, Uri songUri, Context context)
    {
        Song song = null;
        prepareDao(context);

        try {
            song = new Song(songName, songUri);
            Integer id = dao.insertObject(song.getValues());
            song.setId(id);
        } catch (Exception e) {
            Log.i("MyErrorDB", e.getMessage());
            song = null;
        }
        return song;
    }

    //recupera una cancion
    public static Song recoverSong(Integer id, Context context)
    {
        Song song = null;
        prepareDao(context);
        ContentValues[] values = dao.getValues(id + "");
        if (values != null) {
            song = values[0] != null ? new Song(values[0]) : null;
        }
        return song;
    }

    //recupera todas las canciones
    public static List<Song> recoverSongs(Context context)
    {
        List<Song> songs = new ArrayList<Song>();
        prepareDao(context);

        ContentValues[] allValues = dao.getValues();

        if (allValues != null) {
            for (ContentValues values : allValues) {
                songs.add(new Song(values));
            }
        }
        return songs;
    }
    //fin base de datos

    private Song(String songName, Uri songUri)
    {
        this.songName = songName;
        this.songUri = songUri;
    }

    private Song(ContentValues values)
    {
        this.id = values.getAsInteger("id");
        this.songName = values.getAsString("songName");
        this.songUri = Uri.parse(values.getAsString("songUri"));
    }

    private ContentValues getValues()
    {
        ContentValues values = new ContentValues();
        values.put("songName", this.songName);
        values.put("songUri", this.getSongUri().toString());

        return values;
    }

    public boolean deleteThisSong(Context context)
    {
        prepareDao(context);

        return daoRelacional.deleteObject("idSongs", this.getId() + "") ? dao.deleteObject(this.id + "") : false;
    }

    public boolean updateThisSong(Context context)
    {
        prepareDao(context);
        return dao.updateObject(this.getValues(), this.id + "") == 1 ? true : false;
    }

    private void setId(Integer id)
    {
        this.id = id;
    }

    public void setSongName(String name)
    {
        this.songName = name;
    }

    public void setSongUri(Uri songUri)
    {
        this.songUri = songUri;
    }

    public String getSongName() {
        return songName;
    }

    public Uri getSongUri() {
        return songUri;
    }

    public Integer getId()
    {
        return id;
    }

    @Override
    public String toString()
    {
        return "Song name: " + this.getSongName() + " Song Uri: " + this.getSongUri().toString() + " id: " + this.getId();
    }
}
