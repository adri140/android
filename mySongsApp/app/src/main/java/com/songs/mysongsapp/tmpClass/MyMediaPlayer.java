package com.songs.mysongsapp.tmpClass;

import android.app.Activity;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Button;
import android.widget.SeekBar;

import com.songs.mysongsapp.clases.Song;
import com.songs.mysongsapp.clases.SongsList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyMediaPlayer implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {

    private static MyMediaPlayer player;

    private MediaPlayer mediaPlayer;
    private List<Song> songs;
    private SongsList list;
    private Integer numSong = 0;

    private Activity activity;

    private boolean play = false;
    private boolean volumen;

    private boolean infiniteLoop;
    private SeekBar bar;

    private Runnable runnable;
    private Handler handler;

    private Button iniciar;

    public static MyMediaPlayer singelton(Activity activity, List<Song> songs)
    {
        if(player == null)
        {
            player = new MyMediaPlayer(activity, songs);
        }
        return player;
    }

    public Integer getSongId()
    {
        return this.songs.get(numSong).getId();
    }

    public Integer getListId()
    {
        return this.list != null ? this.list.getId() : -1;
    }

    public void setIniciar(Button in)
    {
        this.iniciar = in;
    }

    private MyMediaPlayer(Activity activity, List<Song> songs)
    {
        handler = new Handler();
        this.activity = activity;
        this.songs = songs == null? new ArrayList<Song>() : songs;
        prepareMediaPlayer();
        infiniteLoop = false;
    }

    public boolean getPlay()
    {
        return this.play;
    }

    public MediaPlayer getMediaPlayer()
    {
        return this.mediaPlayer;
    }

    private void prepareMediaPlayer()
    {
        if (songs.size() > 0 && mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(this.activity, songs.get(0).getSongUri());
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnCompletionListener(this);
            prepareVolume();
        }
        else
        {
            if(mediaPlayer == null) {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setOnPreparedListener(this);
                mediaPlayer.setOnCompletionListener(this);
                prepareVolume();
            }
        }
    }

    private void prepareVolume()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        //SharedPreferences.Editor editorpreferencies = prefs.edit();
        boolean song = prefs.getBoolean("songOptionPreference", true);

        if(song){
            mediaPlayer.setVolume(1, 1);
            volumen = true;
        }
        else{
            mediaPlayer.setVolume(0, 0);
            volumen = false;
        }
    }

    public void setInfiniteLoop(boolean infiniteLoop)
    {
        this.infiniteLoop = infiniteLoop;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        if(!mp.isPlaying()) mp.start();
    }

    public void onStoped()
    {
        if(mediaPlayer.isPlaying())
        {
            mediaPlayer.stop();
        }
    }

    public void onPause()
    {
        if(mediaPlayer.isPlaying())
        {
            mediaPlayer.pause();
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        onSiguiente();
    }

    public void addNewSong(Song newSongs)
    {
        if(mediaPlayer != null)
        {
            onStoped();
            mediaPlayer.reset();
        }

        songs.clear();

        this.list = null;
        songs.add(newSongs);
        numSong = 0;

        this.changeSong();
    }

    public void addNewList(SongsList list)
    {
        if(mediaPlayer != null)
        {
            onStoped();
            mediaPlayer.reset();
        }

        songs.clear();

        this.list = list;
        songs = list.getSongs(this.activity);
        numSong = 0;

        this.changeSong();
    }

    public boolean onSiguiente()
    {
        if(numSong == songs.size() - 1)
        {
            numSong = -1;
        }

        if(mediaPlayer != null && mediaPlayer.isPlaying())
        {
            onStoped();
        }

        mediaPlayer.reset();
        numSong++;

        this.changeSong();
        return play;
    }

    private void changeSong()
    {
        try {
            mediaPlayer.setDataSource(this.activity, songs.get(numSong).getSongUri());
            mediaPlayer.prepareAsync();

            play = true;
        } catch (IOException e) { }
        catch (Exception e) { }

    }

    public boolean playSongs()
    {
        if(mediaPlayer != null && songs.size() > 0) {
            if (play) {
                player.onPause();
                play = false;

            } else {
                player.onPrepared(player.getMediaPlayer());
                play = true;

            }
        }
        return play;
    }

    public boolean onAnterior()
    {
        if(numSong == 0)
        {
            numSong = songs.size();
        }

        if(mediaPlayer != null && mediaPlayer.isPlaying())
        {
            onStoped();
        }

        mediaPlayer.reset();
        numSong--;

        return play;
    }

    public boolean modifyVolumen()
    {
        Log.i("pressVolumen", "Volumen");
        if(volumen)
        {
            mediaPlayer.setVolume(0, 0);
            volumen = false;
        }
        else
        {
            mediaPlayer.setVolume(1, 1);
            volumen = true;
        }
        return volumen;
    }

    public boolean getVolumen()
    {
        return this.volumen;
    }

    public Integer getPosition()
    {
        return mediaPlayer.getCurrentPosition();
    }
}
