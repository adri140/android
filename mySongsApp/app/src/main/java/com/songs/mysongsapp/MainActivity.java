package com.songs.mysongsapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.songs.mysongsapp.clases.Song;
import com.songs.mysongsapp.clases.SongsList;
import com.songs.mysongsapp.fragments.FragmentLists;
import com.songs.mysongsapp.fragments.FragmentSongs;
import com.songs.mysongsapp.fragments.FragmentMediaPlayer;
import com.songs.mysongsapp.preferences.ConfigurationActivity;
import com.songs.mysongsapp.tmpClass.MyMediaPlayer;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    public static FragmentManager manager;
    //private FragmentSongs song;
    public static Map<String, Object[]> fragmentos;

    public static MyMediaPlayer player;
    public static Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        player = MyMediaPlayer.singelton(this, null);

        Toolbar tollbar = findViewById(R.id.myToolBar);
        tollbar.setTitle(R.string.app_name);
        tollbar.setTitleMarginStart(30);
        tollbar.setLogo(R.drawable.app_logo);
        setSupportActionBar(tollbar);

        this.prepareFragments();

        Button songs = findViewById(R.id.songsButton);
        Button songsList = findViewById(R.id.listsButton);
        Button play = findViewById(R.id.playButton);

        songs.setOnClickListener(this);
        songsList.setOnClickListener(this);
        play.setOnClickListener(this);
    }

    private void prepareFragments()
    {
        manager = manager == null ? getSupportFragmentManager() : manager;

        fragmentos = new HashMap<String, Object[]>();
        fragmentos.put("SongFragment", new Object[] {new FragmentSongs(), false});
        fragmentos.put("ListFragment", new Object[] {new FragmentLists(), false});
        fragmentos.put("MediaPlayer", new Object[] {new FragmentMediaPlayer(), false});

        FragmentTransaction transaction = manager.beginTransaction();
        Object[] fra = fragmentos.get("SongFragment");
        fra[1] = true;
        transaction.add(R.id.sectionSongs, (Fragment) fra[0]); //cargamos el fragmento con el id del layout donde lo incluiremos
        transaction.commit(); //hacemos commit
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_options, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {

        if(item.getItemId() == R.id.options)
        {
            startActivity(new Intent(this, ConfigurationActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v)
    {
        //carga de fragmentos
        //creamos una transaccion con el manager
        FragmentTransaction transaction = manager.beginTransaction();
        Object[] fra = null;
        switch(v.getId())
        {
            case R.id.songsButton:
                fra = fragmentos.get("SongFragment");
                if(!(Boolean) fra[1]) {
                    clearFragments(transaction);
                    fra[1] = true;
                    transaction.add(R.id.sectionSongs, (Fragment) fra[0]); //cargamos el fragmento con el id del layout donde lo incluiremos
                    transaction.commit(); //hacemos commit
                }
                break;
            case R.id.listsButton:
                fra = fragmentos.get("ListFragment");
                if(!(Boolean) fra[1]) {
                    clearFragments(transaction);
                    fra[1] = true;
                    transaction.add(R.id.sectionSongs, (Fragment) fra[0]); //cargamos el fragmento con el id del layout donde lo incluiremos
                    transaction.commit(); //hacemos commit
                }
                break;
            case R.id.playButton:
                fra = fragmentos.get("MediaPlayer");
                if(!(Boolean) fra[1]) {
                    clearFragments(transaction);
                    fra[1] = true;
                    transaction.add(R.id.sectionSongs, (Fragment) fra[0]); //cargamos el fragmento con el id del layout donde lo incluiremos
                    transaction.commit(); //hacemos commit
                }
                break;
            default:
                Toast.makeText(this, "Accion no controlada", Toast.LENGTH_LONG).show();
        }
    }

    //elimina todos los fragmentos activados
    private void clearFragments(FragmentTransaction transaction)
    {
        //Toast.makeText(this, "cleared", Toast.LENGTH_LONG).show();
        for(String key : fragmentos.keySet()){
            Object[] ob = fragmentos.get(key);
            if((Boolean) ob[1]) {
                transaction.remove((Fragment) ob[0]);
                ob[1] = false;
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        FragmentTransaction transaction = manager.beginTransaction();

        for(String key : fragmentos.keySet()){
            Object[] ob = fragmentos.get(key);
            if((Boolean) ob[1]) {
                transaction.detach((Fragment) ob[0]);
                transaction.attach((Fragment) ob[0]);
                break;
            }
        }
        transaction.commit();
    }

    public void sendSongToMediaPlayer(Song song)
    {
        FragmentTransaction transaction = manager.beginTransaction();
        clearFragments(transaction);

        Object[] fra = fragmentos.get("MediaPlayer");
        fra[1] = true;

        transaction.add(R.id.sectionSongs, (Fragment) fra[0]); //cargamos el fragmento con el id del layout donde lo incluiremos
        transaction.commit(); //hacemos commit

        ((FragmentMediaPlayer) fra[0]).onAttach(getApplicationContext());
        ((FragmentMediaPlayer) fra[0]).changeSong(song);
    }

    public void sendSongsListToMediaPlayer(SongsList list)
    {
        FragmentTransaction transaction = manager.beginTransaction();
        clearFragments(transaction);

        Object[] fra = fragmentos.get("MediaPlayer");
        fra[1] = true;
        transaction.add(R.id.sectionSongs, (Fragment) fra[0]); //cargamos el fragmento con el id del layout donde lo incluiremos
        transaction.commit(); //hacemos commit

        ((FragmentMediaPlayer) fra[0]).onAttach(getApplicationContext());
        ((FragmentMediaPlayer) fra[0]).changeList(list);
    }
}
