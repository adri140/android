package com.songs.mysongsapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.songs.mysongsapp.clases.Song;

public class AddSongsActivity extends Activity implements View.OnClickListener
{
    private Uri selectedUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_song);

        prepareButtons();
    }

    private void prepareButtons()
    {
        Button returnButton = findViewById(R.id.returnButton);
        Button searchSong = findViewById(R.id.searchButton);
        Button addSong = findViewById(R.id.addSongButton);

        returnButton.setOnClickListener(this);
        searchSong.setOnClickListener(this);
        addSong.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.searchButton:
                // un intent a action_open_document
                Intent search = new Intent(Intent.ACTION_OPEN_DOCUMENT);

                // añade la categoria de que sea openable
                search.addCategory(Intent.CATEGORY_OPENABLE);

                // Filtra para que solo se acepten imagenes
                search.setType("audio/mpeg");

                startActivityForResult(search, 42);
                break;
            case R.id.addSongButton:
                this.createSong();
                finishThis();
                break;
            case R.id.returnButton:
                finishThis();
                break;
                default:
                    Toast.makeText(this, "opcion no controlada", Toast.LENGTH_SHORT).show();
        }
    }

    private void createSong()
    {
        EditText nameSong = findViewById(R.id.nameSong);
        Song.createSong(nameSong.getText().toString(), selectedUri, this);
    }

    private void finishThis()
    {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case 42:
                if(data != null)
                {
                    selectedUri = data.getData();
                    String finalRuta = selectedUri.getLastPathSegment();

                    TextView ruta = findViewById(R.id.rutaText);
                    ruta.setText(finalRuta);

                    finalRuta = finalRuta.substring(finalRuta.lastIndexOf('/') + 1, finalRuta.length());

                    EditText nameSong = findViewById(R.id.nameSong);
                    nameSong.setText(finalRuta);
                }
                break;
        }
    }
}
