package recio.adrian.sqlite.ttb_api16;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.SQLException;

import recio.adrian.sqlite.ttb_api16.DataBase.Data;
import recio.adrian.sqlite.ttb_api16.ttb.Cliente;

public class LoggedUserActivity extends AppCompatActivity implements View.OnClickListener {

    /*private Cliente cli;
    private Context ctx;
    private Cursor mcursor;*/
    private CheckBox check_nombre, check_password;
    private EditText edituser, editPassword;

    //private Data helper = new Data(this);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.iniciar);

        Button b_closeSession = findViewById(R.id.botAtras);
        //Button b_configUser = findViewById(R.id.botAjustes);
        Button b_enviar = findViewById(R.id.botEnviar);

        b_closeSession.setOnClickListener(this);
        //b_configUser.setOnClickListener(this);
        b_enviar.setOnClickListener(this);

        edituser = (EditText)findViewById(R.id.editUser);
        editPassword = (EditText)findViewById(R.id.editPassword);
        check_nombre = (CheckBox)findViewById(R.id.checkNom);
        check_password = (CheckBox)findViewById(R.id.checkPass);

        CargarChecked();
        CargarDatos();
    }

    @Override
    public void onClick(View v)
    {
        if(v instanceof Button){
            Button b_tmp = (Button) v;

            Intent intent;

            switch(b_tmp.getId()){
                case R.id.botAtras:
                    //vuelves atras
                    this.setResult(RESULT_OK);
                    this.finish();
                    break;

                /*case R.id.botAjustes:
                    break;*/

                case R.id.botEnviar:
                    String nombre = edituser.getText().toString();
                    String contraseña = editPassword.getText().toString();

                    if(check_password.isChecked()){
                        check_nombre.setChecked(true);
                    }

                    GuardarPreferencias();

                    if(validarString(nombre) && validarString(contraseña)){
                            /*Cursor cursor = helper.ConsultarUsuPas(edituser.getText().toString(), editPassword.getText().toString());
                            if (cursor.getCount() > 0) {
                                cli = Cliente.recoverCliente(String.valueOf(edituser.getText()), this);

                                intent = new Intent(this, ControlActivity.class);
                                intent.putExtra("CLIENTE", this.cli);
                                startActivityForResult(intent, 1, null);
                            } else {
                                Toast.makeText(getApplicationContext(), "Usuario y/o Password no existen!!",
                                        Toast.LENGTH_LONG).show();

                                edituser.setText("");
                                editPassword.setText("");
                                check_nombre.setChecked(false);
                                check_password.setChecked(false);
                                edituser.findFocus();
                            }*/

                        //para comprovar que dos strings sean iguales en cuanto a contenido se utiliza el methodo string.equals(string)
                        Cliente e = Cliente.recoverCliente(edituser.getText().toString(), this);
                        if(e != null) {
                            if (e.getPassword().trim().equals(editPassword.getText().toString().trim())) {
                                intent = new Intent(this, LoggedUserMainActivity.class);
                                intent.putExtra("CLIENTE", e);
                                startActivityForResult(intent, 1, null);
                            } else {
                                Toast.makeText(getApplicationContext(), "Usuario y/o Password no existen!!",
                                        Toast.LENGTH_LONG).show();

                                edituser.setText("");
                                editPassword.setText("");
                                check_nombre.setChecked(false);
                                check_password.setChecked(false);
                                edituser.findFocus();
                            }
                        }
                        else{
                            Toast.makeText(getApplicationContext(), "El usuario no existen!!",
                                    Toast.LENGTH_LONG).show();
                        }
                    } else{
                        Toast.makeText(getApplicationContext(), "Datos incompletos", Toast.LENGTH_SHORT).show();
                    }
                    break;

                default:
            }
        }
    }

    public void CargarChecked(){
        SharedPreferences mispreferencias = getSharedPreferences("PreferenciasUsuarios", Context.MODE_PRIVATE);
        check_nombre.setChecked(mispreferencias.getBoolean("checknom", false));
        check_password.setChecked(mispreferencias.getBoolean("checkpass", false));
    }

    public void CargarDatos(){
        SharedPreferences mispreferencias = getSharedPreferences("PreferenciasUsuarios", Context.MODE_PRIVATE);

        if (check_nombre.isChecked()){
            edituser.setText(mispreferencias.getString("nombre", ""));
        }
        if(check_password.isChecked()){
            editPassword.setText(mispreferencias.getString("contraseña", ""));
        }
    }

    public void GuardarPreferencias(){
        SharedPreferences mispreferencias = getSharedPreferences("PreferenciasUsuarios", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mispreferencias.edit();

        if(check_nombre.isChecked()){
            String nombre = edituser.getText().toString();
            boolean checknom = check_nombre.isChecked();
            editor.putString("nombre",nombre);
            editor.putBoolean("checknom",checknom);
            edituser.setText(mispreferencias.getString("nombre", ""));
            if(check_password.isChecked()){
                String contraseña = editPassword.getText().toString();
                boolean checkpass = check_password.isChecked();
                editor.putString("contraseña", contraseña);
                editor.putBoolean("checkpass",checkpass);
                editPassword.setText(mispreferencias.getString("contraseña", ""));
            } else {
                editor.remove("contraseña");
                editor.remove("checkpass");
            }
        }
        else {
            editor.remove("nombre");
            editor.remove("checknom");
            editor.remove("contraseña");
            editor.remove("checkpass");
        }
        editor.commit();
    }

    boolean validarString(String texto){
        return texto!=null && texto.trim().length()>0;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null){//si se a pulsado a return data sera null, si se a pulsado a cerrar session data no sera nullo.
            Integer exit = data.getIntExtra("EXIT", 0);
            if(exit == 1){
                this.setResult(RESULT_OK, data);
                this.finish();
            }
        }
    }
}
