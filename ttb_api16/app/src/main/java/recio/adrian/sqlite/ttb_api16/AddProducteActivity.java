package recio.adrian.sqlite.ttb_api16;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import recio.adrian.sqlite.ttb_api16.ttb.Cliente;
import recio.adrian.sqlite.ttb_api16.ttb.Comanda;
import recio.adrian.sqlite.ttb_api16.ttb.Producte;

public class AddProducteActivity extends Activity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private String[] names;
    private Integer option = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.producte_add);

        this.addSpinner();

        Button botReturn = findViewById(R.id.botReturn);
        botReturn.setOnClickListener(this);

        Button botEnviar = findViewById(R.id.botEnviar);
        botEnviar.setOnClickListener(this);
    }

    private void returnProducte(){
        EditText e_cantidad = findViewById(R.id.numberCantidad);
        ListView list = findViewById(R.id.producteList);

        if(option >= 0 && e_cantidad.getText().toString().length() > 0) {
            Producte pp = Producte.recoverOneProductWithName(this.names[option], this);
            Intent intent = new Intent();
            intent.putExtra("PRODUCTE", this.names[option]);
            intent.putExtra("CANTIDAD", Integer.parseInt(e_cantidad.getText().toString().trim()));

            this.setResult(RESULT_OK, intent);
            finish();
        }
        else{
            Toast.makeText(this, "Faltan datos por rellenar!", Toast.LENGTH_LONG).show();
        }
    }

    private void addSpinner(){
        Producte[] productes = Producte.recoverProducteForCreateComanda(this);
        names = new String[productes.length];
        int i = 0;
        for(Producte pro: productes){
            names[i] = pro.getNom_Producte();
            i++;
        }

        Spinner productoSpinner = findViewById(R.id.productoSpinner);
        productoSpinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, names));
        productoSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        this.option = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if(v instanceof  Button){
            Button tmp = (Button) v;
            switch(tmp.getId()){
                case R.id.botEnviar:
                    returnProducte();
                    break;
                case R.id.botReturn:
                    setResult(RESULT_OK);
                    finish();
                    break;
            }
        }
    }
}
