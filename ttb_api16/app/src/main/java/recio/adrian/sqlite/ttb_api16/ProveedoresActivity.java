package recio.adrian.sqlite.ttb_api16;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ProveedoresActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.proveedores);

        Button b_atras = findViewById(R.id.botAtras);
        Button b_configUser = findViewById(R.id.botAjustes);

        b_atras.setOnClickListener(this);
        b_configUser.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v instanceof Button){
            Button b_tmp = (Button) v;

            Intent intent;

            switch(b_tmp.getId()){
                case R.id.botAtras:
                    //vuelves atras
                    this.setResult(RESULT_OK);
                    this.finish();
                    break;
                case R.id.botAjustes:
                    break;

                default:
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null){//si se a pulsado a return data sera null, si se a pulsado a cerrar session data no sera nullo.
            Integer exit = data.getIntExtra("EXIT", 0);
            if(exit == 1){
                this.setResult(RESULT_OK, data);
                this.finish();
            }
        }
    }
}
