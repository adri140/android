package recio.adrian.sqlite.ttb_api16;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import recio.adrian.sqlite.ttb_api16.ttb.Cliente;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {

    //private Cliente cli;
    private EditText edituser, editpassword, editNIF;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);

        Button b_closeSession = findViewById(R.id.botAtras);
        //Button b_configUser = findViewById(R.id.botAjustes);
        Button b_enviar = findViewById(R.id.botEnviar);

        b_closeSession.setOnClickListener(this);
        //b_configUser.setOnClickListener(this);
        b_enviar.setOnClickListener(this);

        edituser = (EditText) findViewById(R.id.editUser);
        editNIF = (EditText) findViewById(R.id.editNIF);
        editpassword = (EditText) findViewById(R.id.editPassword);
    }

    @Override
    public void onClick(View v)
    {
        if(v instanceof Button){
            Button b_tmp = (Button) v;

            Intent intent;

            switch(b_tmp.getId()){
                case R.id.botAtras:
                    //vuelves atras
                    this.setResult(RESULT_OK);
                    this.finish();
                    break;
                /*case R.id.botAjustes:
                    break;*/
                case R.id.botEnviar:
                    if(validarString(editNIF.getText().toString()) && validarString(editpassword.getText().toString())){
                        Cliente cli = Cliente.createCliente(String.valueOf(edituser.getText()),
                                String.valueOf(editNIF.getText()),
                                String.valueOf(editpassword.getText()),
                                this);

                        Toast.makeText(getApplicationContext(),"Registro Almacenado con Exito",
                                Toast.LENGTH_LONG).show();

                        /*Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);*/
                        setResult(RESULT_OK);
                        finish();
                    } else{
                        Toast.makeText(getApplicationContext(), "Datos incompletos", Toast.LENGTH_SHORT).show();
                    }break;

                default:
            }
        }
    }

    boolean validarString(String texto){
        return texto!=null && texto.trim().length()>0;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null){//si se a pulsado a return data sera null, si se a pulsado a cerrar session data no sera nullo.
            Integer exit = data.getIntExtra("EXIT", 0);
            if(exit == 1){
                this.setResult(RESULT_OK, data);
                this.finish();
            }
        }
    }
}
