package recio.adrian.sqlite.ttb_api16;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Date;

import recio.adrian.sqlite.ttb_api16.ttb.Cliente;
import recio.adrian.sqlite.ttb_api16.ttb.Comanda;
import recio.adrian.sqlite.ttb_api16.ttb.Enum_Tipus;
import recio.adrian.sqlite.ttb_api16.ttb.Enum_Unitat;
import recio.adrian.sqlite.ttb_api16.ttb.Producte;

public class LoggedUserMainActivity extends AppCompatActivity implements View.OnClickListener {

    private Cliente cli;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.panel_control);

        Button b_closeSession = findViewById(R.id.botSession);
        Button b_configUser = findViewById(R.id.botUser);
        Button b_viewPedidos = findViewById(R.id.botPaquete);

        b_closeSession.setOnClickListener(this);
        b_configUser.setOnClickListener(this);
        b_viewPedidos.setOnClickListener(this);

        Intent intent = getIntent();
        cli = (Cliente) intent.getSerializableExtra("CLIENTE");
    }

    @Override
    public void onClick(View v)
    {
        if(v instanceof Button){
            Button b_tmp = (Button) v;

            Intent intent;

            switch(b_tmp.getId()){
                case R.id.botSession:
                    //te envia al main, es decir, ciera este activity y el de login o el de registrar-se
                    intent = new Intent();
                    intent.putExtra("EXIT", 1);
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                case R.id.botUser:
                    //te envia a la configuracion del cliente
                    if(this.cli != null) {
                        intent = new Intent(this, PerfilActivity.class);
                        intent.putExtra("CLIENTE", this.cli);
                        startActivityForResult(intent, 1, null);
                    }
                    else{
                        Log.i("MyUserError", "Error: El usuario no se a logeado.");
                    }
                    break;
                case R.id.botPaquete:
                    //te envia a ver los pedidos del cliente
                    if(this.cli != null) {
                        intent = new Intent(this, ComandaActivity.class);
                        intent.putExtra("CLIENTE", this.cli);
                        startActivityForResult(intent, 1, null);
                    }
                    else{
                        Log.i("MyUserError", "Error: El usuario no se a logeado.");
                    }
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null){//si se a pulsado a return data sera null, si se a pulsado a cerrar session data no sera nullo.
            Integer exit = data.getIntExtra("EXIT", 0);
            if(exit == 1){
                this.setResult(RESULT_OK, data);
                this.finish();
            }
        }
    }
}
