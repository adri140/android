package recio.adrian.sqlite.ttb_api16;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Map;

import recio.adrian.sqlite.ttb_api16.ttb.Producte;

public class ComandaAdapterProducte extends ArrayAdapter {

    private Activity context;
    private Map<Producte, Integer> productes;

    public ComandaAdapterProducte(Activity context, Map<Producte, Integer> productes) {
        super(context, R.layout.comanda_producte_list, productes.keySet().toArray());
        this.context = context;
        this.productes = productes;
    }

    @Override
    public View getView(int position,View convertView, ViewGroup parent) {
        LayoutInflater l_inflater = context.getLayoutInflater();

        View item = l_inflater.inflate(R.layout.comanda_producte_list, null);
        if(this.productes.size() > 0) {

            TextView productName = item.findViewById(R.id.productName);
            Producte e = (Producte) this.productes.keySet().toArray()[position];
            productName.setText("Prod: " + e.getNom_Producte());
            TextView cantidad = item.findViewById(R.id.cantidad);
            cantidad.setText("Cant: " + this.productes.get(e) + "");

        }
        return (item);
    }
}
