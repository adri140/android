package recio.adrian.sqlite.ttb_api16;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import recio.adrian.sqlite.ttb_api16.ttb.Cliente;
import recio.adrian.sqlite.ttb_api16.ttb.Comanda;
import recio.adrian.sqlite.ttb_api16.ttb.Producte;

public class ComandaViewActivity<fchDateSetListener> extends Activity implements  View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private Cliente cli;
    private Comanda com;
    private Map<Producte, Integer> producte;
    private ComandaAdapterProducte c_adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comanda_view);

        //recojemos el cliente con el que estamos trabajando
        Intent intent = getIntent();
        cli = (Cliente) intent.getSerializableExtra("CLIENTE");

        if(cli != null){
            com = (Comanda) intent.getSerializableExtra("COMANDA");

            TextView idComanda = findViewById(R.id.textIdComanda);
            TextView comandafch = findViewById(R.id.comandafch);
            TextView fchlimit = findViewById(R.id.fchlimit); //limite
            TextView textComandaEstat = findViewById(R.id.textComandaEstat);

            TextView totalPortes = findViewById(R.id.textTotalPortes);

            Button addProducte = findViewById(R.id.addProducte);
            addProducte.setOnClickListener(this);

            Button botEnviar = findViewById(R.id.botEnviar);
            botEnviar.setOnClickListener(this);

            Button botSession = findViewById(R.id.botSession);
            botSession.setOnClickListener(this);

            Button botReturn = findViewById(R.id.botReturn);
            botReturn.setOnClickListener(this);


            //si la comanda no es nulla cargamos todos los datos de la comanda
            if(com != null){
                producte = com.getProductos();
                idComanda.setText(com.getId() + "");

                SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
                String newDateStr = curFormater.format(com.getData_peticio());
                comandafch.setText(newDateStr);

                newDateStr = curFormater.format(com.getData_Limit());
                fchlimit.setText(newDateStr);

                totalPortes.setText(com.getPorts() + "€");
                textComandaEstat.setText(com.getEstat().toString());
                addProducte.setEnabled(false);
                botEnviar.setEnabled(false);
            }
            else{ //si la comanda es nulla solo cargamos los productos de la base de datos
                producte = new HashMap<Producte, Integer>();
                fchlimit.setOnClickListener(this);
                totalPortes.setText(0.00 + "€");
                textComandaEstat.setText("PENDIENTE");
            }
           this.createAdapter();

        }
        else{
            Log.i("MyErrorActivity", "El cliente no existe.");
        }
    }

    //comprueva que estan bien los datos, despues guarda la comanda en la base de datos y cierra el activity
    private void saveMethode(){
        TextView totalPortes = findViewById(R.id.textTotalPortes);
        Double price = Double.parseDouble(totalPortes.getText().toString().trim().substring(0, totalPortes.getText().toString().trim().length() - 1));
        TextView fchlimit = findViewById(R.id.fchlimit);
        String lmt = fchlimit.getText().toString();

        if(lmt.length() > 0 && this.producte.size() > 0) {
            String[] date = lmt.split("-");

            Integer year = Integer.parseInt(date[0].trim());
            year = year - 1900;
            Integer month = Integer.parseInt(date[1].trim());
            Integer day = Integer.parseInt(date[2].trim());

            Date limit = new Date(year, month, day);
            if (compareDate(limit) > 0) {
                this.com = Comanda.createComanda(limit, 0.0, cli, this);

                Iterator<Producte> er = this.producte.keySet().iterator();
                while (er.hasNext()) {
                    Producte p = er.next();
                    Integer cantidad = this.producte.get(p);
                    this.com.addProducte(p, cantidad, this);
                }

                this.setResult(RESULT_OK);
                this.finish();
            }
            else{
                Toast.makeText(this, "No hay suficiente margen para la entrega, elije otra fecha.", Toast.LENGTH_LONG).show();
            }

        }
        else{
            Toast.makeText(this, "Faltan campos por rellenar.", Toast.LENGTH_LONG).show();
        }
    }

    private Integer compareDate(Date limit){
        Date today = new Date();
        Integer e = -1;

        System.out.println(today.getYear() + " " + limit.getYear());
        if(today.getYear() <= limit.getYear()) {
            if (today.getMonth() <= limit.getMonth()) {
                System.out.println(today.getDate() + " " + limit.getDate());
                if (today.getDate() < limit.getDate()) {
                    e = 1;
                }
            }
        }
        return e;
    }

    @Override
    public void onClick(View v) {
        if(v instanceof Button){
            Button anonimous = (Button) v;
            Intent intent;
            switch(anonimous.getId()){
                case R.id.addProducte:
                    intent = new Intent(this, AddProducteActivity.class);
                    startActivityForResult(intent, 1, null);
                    break;
                case R.id.botEnviar:
                    this.saveMethode();
                    break;
                case R.id.botReturn:
                    setResult(RESULT_OK);
                    finish();
                    break;
                case R.id.botSession:
                    intent = new Intent();
                    intent.putExtra("EXIT", 1);
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
            }
        }
        else{
            if(v instanceof TextView){
                TextView tt = (TextView) v;

                switch(tt.getId()){ //preparamos y habrimos el calendario
                    case R.id.fchlimit:
                        Calendar cal =  Calendar.getInstance();
                        int year = cal.get(Calendar.YEAR);
                        int month = cal.get(Calendar.MONTH);
                        int day = cal.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog dialog = new DatePickerDialog(this, this, year, month, day);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.GRAY));
                        dialog.show();

                        break;
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null){//si se a pulsado a return data sera null, si se a pulsado a cerrar session data no sera nullo.

            String prodName = data.getStringExtra("PRODUCTE");
            Integer cantidad = data.getIntExtra("CANTIDAD", 1);

            Producte pp = Producte.recoverOneProductWithName(prodName, this);
            this.producte.put(pp, cantidad);

            Iterator<Producte> er = this.producte.keySet().iterator();
            while(er.hasNext()){
                Producte p = er.next();
            }

            Double priceTotal = 0.0;
            priceTotal = Producte.recoverOneProductWithName(prodName, this).getPreu() * cantidad;

            TextView tt = findViewById(R.id.textTotalPortes);

            priceTotal += Double.parseDouble(tt.getText().toString().trim().substring(0, tt.length() - 1));
            tt.setText(priceTotal + "" + "€");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.createAdapter();
    }

    //crea el adaptador
    private void createAdapter(){
        ListView prod = findViewById(R.id.producteList);
        prod.clearChoices();
        prod.setAdapter(null);
        this.c_adapter = new ComandaAdapterProducte(this, this.producte);
        prod.setAdapter(this.c_adapter);
    }

    //al hacer click en ok del calendario hacemos esto
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        TextView fchlimit = findViewById(R.id.fchlimit);
        fchlimit.setText(year + "-" + (month + 1)+ "-" + dayOfMonth);
    }
}
