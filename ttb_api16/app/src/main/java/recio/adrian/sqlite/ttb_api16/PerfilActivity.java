package recio.adrian.sqlite.ttb_api16;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import recio.adrian.sqlite.ttb_api16.DataBase.Data;
import recio.adrian.sqlite.ttb_api16.ttb.Cliente;

public class PerfilActivity extends Activity implements View.OnClickListener {

    private Cliente cli;
    private String texto;
    private TextView nombre;
    private EditText oldpass, newpass;
    //private Data helper = new Data(this);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfil);

        Button b_return = findViewById(R.id.botAtras);
        Button b_close = findViewById(R.id.botSession);
        Button b_enviar = findViewById(R.id.botEnviar);

        b_return.setOnClickListener(this);
        b_close.setOnClickListener(this);
        b_enviar.setOnClickListener(this);

        Intent intent = getIntent();
        cli = (Cliente) intent.getSerializableExtra("CLIENTE");


        nombre = (TextView)findViewById(R.id.editNomPerfil) ;
            nombre.setText(cli.getNom());

        oldpass = (EditText)findViewById(R.id.editContra) ;
            oldpass.setText(cli.getPassword());

        if(cli != null) {
            this.cli = cli;
        }
    }

    @Override
    public void onClick(View v)
    {
        if(v instanceof Button){
            Button b_tmp = (Button) v;

            Intent intent;

            switch(b_tmp.getId()){
                case R.id.botAtras:
                    //vuelves atras
                    this.setResult(RESULT_OK);
                    this.finish();
                    break;

                case R.id.botSession:
                    intent = new Intent();
                    intent.putExtra("EXIT", 1);
                    this.setResult(RESULT_OK, intent);
                    this.finish();
                    break;

                case R.id.botEnviar:
                    if(cli != null) {
                        this.cli.setPassword(String.valueOf(oldpass.getText()));
                        boolean update = this.cli.update(this);

                        if (validarString(oldpass.getText().toString())) {
                            if (update) {
                                Toast.makeText(getApplicationContext(), "Contraseña Actualizada",
                                        Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Contraseña ERROR",
                                        Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Datos incompletos", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                default:
            }
        }
    }

    boolean validarString(String texto){
        return texto!=null && texto.trim().length()>0;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null){//si se a pulsado a return data sera null, si se a pulsado a cerrar session data no sera nullo.
            Integer exit = data.getIntExtra("EXIT", 0);
            if(exit == 1){
                this.setResult(RESULT_OK, data);
                this.finish();
            }
        }
    }
}
