package recio.adrian.sqlite.ttb_api16.ttb;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import recio.adrian.sqlite.ttb_api16.DataBase.Dao;

public class Producte implements Serializable {

    private Integer id = 0;
    private String nom_producte;
    private String descripcio_producte;
    private double preu;
    private Integer stock;
    private Integer stock_minim;
    private Enum_Tipus tipus_producte;
    private Enum_Unitat unitat_mesura;

    private Producte(String nom_Producte, String descripcio_Producte, double price, Integer stock, Integer stock_minim, Enum_Tipus tipus_Producte, Enum_Unitat unitat_mesura)
    {
        this.nom_producte = nom_Producte;
        this.descripcio_producte = descripcio_Producte;
        this.preu = price;
        this.stock = stock;
        this.stock_minim = stock_minim;
        this.tipus_producte = tipus_Producte;
        this.unitat_mesura = unitat_mesura;
    }

    private Producte(Integer idProducte, String nom_Producte, String descripcio_Producte, double price, Integer stock, Integer stock_minim, Enum_Tipus tipus_Producte, Enum_Unitat unitat_mesura)
    {
        this.id = idProducte;
        this.nom_producte = nom_Producte;
        this.descripcio_producte = descripcio_Producte;
        this.preu = price;
        this.stock = stock;
        this.stock_minim = stock_minim;
        this.tipus_producte = tipus_Producte;
        this.unitat_mesura = unitat_mesura;
    }

    private void setId(Integer id){
        this.id = id;
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getNom_Producte()
    {
        return this.nom_producte;
    }

    public String getDescripcio_Producte()
    {
        return this.descripcio_producte;
    }

    public double getPreu()
    {
        return this.preu;
    }

    public Integer getStock()
    {
        return this.stock;
    }

    public Integer getStock_Minim()
    {
        return this.stock_minim;
    }

    public Enum_Tipus getTipus_Producte()
    {
        return this.tipus_producte;
    }

    public Enum_Unitat getUnitat_Mesura()
    {
        return this.unitat_mesura;
    }

    public String toString()
    {
        return "nom producte: " + this.getNom_Producte() + " idProducte: " + this.getId() + " descripcio producte: " + this.getDescripcio_Producte();
    }

    //crea un nuevo producto y lo almacena en la base de datos
    public static Producte createProducte(String nom, String descripcio, double price, Integer stock, Integer stock_minim, Enum_Tipus tipus_Producte, Enum_Unitat unitat_mesura, Context context){

        Producte pro = new Producte(nom, descripcio, price, stock, stock_minim, tipus_Producte, unitat_mesura);
        Dao dao = new Dao(context, "Producte", "id");
        pro.setId(dao.insertObject(pro.getValues())); //recogemos el id del producto

        return pro;
    }

    //extrae los productos de una comanda
    //devuelve todos los productos, y sus cantidades de una comanda
    public static Map<Producte, Integer> recoverProducte(Context context, Comanda com){

        Dao dao = new Dao(context, "Comanda_Producte", "idComanda");
        ContentValues[] allValues = dao.getValues(com.getId() + "");
        Map<Producte, Integer> productes = null;

        if(allValues != null){
            productes = new HashMap<Producte, Integer>();
            if(allValues[0] != null){
                for(int p = 0; p < allValues.length; p++){
                    Producte tmp = recoverOneProducte(context, Integer.parseInt((String) allValues[p].get("idProducte")));
                    productes.put(tmp, Integer.parseInt( allValues[p].get("cantidad").toString().trim()));
                }
            }
        }
        return productes;
    }

    //devuelve un producto a partir de su id
    private static Producte recoverOneProducte(Context context, Integer id){
        Dao dao = new Dao(context, "Producte", "id");
        ContentValues[] allValues = dao.getValues(id + "");

        Producte[] productos = getProducte(allValues);

        if(productos != null) return productos[0];
        return null;
    }

    //extrae los productos vendibles de la tabla producte
    public static Producte[] recoverProducteForCreateComanda(Context context){

        Dao dao = new Dao(context, "Producte", "tipus_producte");
        ContentValues[] allValues = dao.getValues("VENDIBLE");
        Producte[] productes = getProducte(allValues);

        return productes;
    }

    //devuelve un objeto producto en formato ContentValues
    private final ContentValues getValues(){
        ContentValues values = new ContentValues();

        if(this.getId() > 0) values.put("id", this.getId());
        values.put("nom_producte", this.getNom_Producte());
        values.put("descripcio_producte", this.getDescripcio_Producte());
        values.put("preu", this.getPreu());
        values.put("stock", this.getStock());
        values.put("stock_minim", this.getStock_Minim());
        values.put("tipus_producte", this.getTipus_Producte().toString());
        values.put("unitat_mesura", this.getUnitat_Mesura().toString());

        return values;
    }

    //transforma un producto de contentValues a producto
    private static final Producte[] getProducte(ContentValues[] allValues){

        Producte[] productes = null;

        if(allValues != null){
            productes = new Producte[allValues.length];

            if(allValues[0] != null){
                for(int p = 0; p < allValues.length; p++){
                    productes[p] = new Producte(Integer.parseInt((String) allValues[p].get("id")), (String) allValues[p].get("nom_producte"), (String) allValues[p].get("descripcio_producte"), Double.parseDouble((String)allValues[p].get("preu")), Integer.parseInt((String) allValues[p].get("stock")), Integer.parseInt((String) allValues[p].get("stock_minim")), Enum_Tipus.valueOf((String) allValues[p].get("tipus_producte")), Enum_Unitat.valueOf((String) allValues[p].get("unitat_mesura")));
                }
            }
        }
        return productes;
    }

    //recupera un producto a partir del nombre de producto
    public static Producte recoverOneProductWithName(String name, Context context){
        Dao dao = new Dao(context, "Producte", "nom_producte");

        ContentValues[] values = dao.getValues(name);

        Producte[] pro = getProducte(values);

        if(pro != null){
            return pro[0];
        }
        return null;
    }
}
