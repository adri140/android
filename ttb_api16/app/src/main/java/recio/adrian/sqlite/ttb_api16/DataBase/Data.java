package recio.adrian.sqlite.ttb_api16.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import recio.adrian.sqlite.ttb_api16.ttb.Enum_Tipus;
import recio.adrian.sqlite.ttb_api16.ttb.Enum_Unitat;
import recio.adrian.sqlite.ttb_api16.ttb.Producte;

public class Data extends SQLiteOpenHelper {

    private static Data database;
    //private static SQLiteDatabase db;

    private static final String userTable = "CREATE TABLE Cliente(" +
            "cif Varchar(9) primary key," +
            "nom Varchar(90) not null UNIQUE," +
            "password Varchar(130) not null," +
            "estat Boolean" +
            ");";
    private static final String comandaTable = "CREATE TABLE Comanda(" +
            "id Integer primary key AUTOINCREMENT,"+
            "data_peticio date not null,"+
            "data_limit date not null,"+
            "estat Varchar(50) default 'PENDENT',"+
            "ports Double,"+
            "cliente Varchar(9),"+
            "foreign key (cliente) references Cliente(cif)"+
            ");";

    private static final String producteTable = "CREATE TABLE Producte("+
            "id Integer primary key AUTOINCREMENT,"+
            "nom_producte Varchar(80) not null UNIQUE,"+
            "descripcio_producte Text,"+
            "preu Double,"+
            "stock Integer,"+
            "stock_minim Integer,"+
            "tipus_producte Varchar(50),"+
            "unitat_mesura Varchar(50)"+
            ");";

    private static final String comanda_producteTable = "CREATE TABLE Comanda_Producte("+
            "idComanda Integer,"+
            "idProducte Integer,"+
            "cantidad Integer,"+
            "primary key (idComanda, idProducte),"+
            "foreign key (idComanda) references Comanda(id),"+
            "foreign key (idProducte) references Producte(id)"+
            ");";

    private static final String DATABASE_NAME = "ttb"; //nombre de la base de datos
    private static final int DATABASE_VERSION = 2;//version de la base de datos

    protected static Data Singelton_Data (Context context){
        if(database == null){
            database = new Data(context);
        }
        return database;
    }

    private Data(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(userTable);
        db.execSQL(comandaTable);
        db.execSQL(producteTable);
        db.execSQL(comanda_producteTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion != newVersion) {

            db.execSQL("DROP TABLE IF EXISTS Comanda_Producte;");
            db.execSQL("DROP TABLE IF EXISTS Comanda;");
            db.execSQL("DROP TABLE IF EXISTS Cliente;");
            db.execSQL("DROP TABLE IF EXISTS Producte;");

            this.onCreate(db);
        }
    }

    public static void insertProducte(Context context){
        SQLiteDatabase db = Singelton_Data(context).getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM Producte", null);

        if(c != null && c.getCount() == 0) {
            Producte.createProducte("Vaso de Lecha", "Un vaso de leche", 4.20, 40, 10, Enum_Tipus.VENDIBLE, Enum_Unitat.LLITROS, context);
            Producte.createProducte("Tarta de Turrón", "Tarta de turrón con almendras", 10.20, 40, 10, Enum_Tipus.VENDIBLE, Enum_Unitat.LLITROS, context);
            Producte.createProducte("Tarta de manzana", "Tarta de manzana", 9.20, 40, 10, Enum_Tipus.VENDIBLE, Enum_Unitat.LLITROS, context);
            Producte.createProducte("Pastel de Chocolate", "Pastel de Chocolate", 12.20, 40, 10, Enum_Tipus.VENDIBLE, Enum_Unitat.LLITROS, context);
            Producte.createProducte("Zumo de Piña", "Zumo de Piña embasada", 4.60, 40, 10, Enum_Tipus.VENDIBLE, Enum_Unitat.LLITROS, context);
        }
    }
}
