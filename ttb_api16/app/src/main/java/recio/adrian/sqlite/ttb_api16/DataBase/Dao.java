package recio.adrian.sqlite.ttb_api16.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Dao {

    private String table; //nombre de la tabla
    private String id; //nombre de la columna por la que realizaras las busquedas

    private static Data bbdd;

    //id es igual a la columna por la que realizaras las búsquedas, no tiene por que ser el id de la tabla
    //table es el nombre de la tabla
    //con es el context que usara para crear el objeto Data, el cual se conectara a la base de datos.
    public Dao(Context con, String table, String id){
        if(bbdd == null) this.bbdd = Data.Singelton_Data(con);
        this.table = table;
        this.id = id;
    }

    //le envías un ContentValues con los atributos del objeto, el nombre de cada valor debe ser el mismo que en la base de datos, este se ocupa de insertar los que hay en el content value, los que no están los añade como nulo
    public Integer insertObject(ContentValues values){
        Integer id = 0;
        SQLiteDatabase db  = bbdd.getWritableDatabase();
        try{
            long l = db.insertOrThrow(table, null, values); //devuelve el id
            id = (int) (long) l;
        }
        catch(Exception e){
            Log.i("MyErrorDB", "InsertError: " + e.getMessage());
        }
        finally{
            db.close();
        }
        return id;
    }

    //devuelve un content value de la tabla de la que quieres obtener datos
    public ContentValues[] getValues(String...ids){
        ContentValues[] allValues = null;

        SQLiteDatabase db = bbdd.getReadableDatabase();

        try{
            String sql = "SELECT * FROM " + table + " WHERE " + id + " = ?";

            Cursor c_cursor = db.rawQuery(sql, ids);

            if(c_cursor.moveToFirst()){
                allValues = new ContentValues[c_cursor.getCount()];
                int i = 0;
                do{
                    allValues[i] = new ContentValues();
                    for(Integer p = 0; p < c_cursor.getColumnCount(); p++){
                        String columnName = c_cursor.getColumnName(p);
                        //menuda mierda, no hay para extraer un Date, veras tu para sacar-lo de la base de datos
                        String value = c_cursor.getString(p);

                        allValues[i].put(columnName, value);
                    }
                    i++;
                }while(c_cursor.moveToNext());
            }
        }
        catch(Exception e){
            Log.i("MyErrorDB", "Error: " + e.getMessage());
        }
        finally {
            db.close();
        }
        return allValues;
    }

    //actualiza un registro de la base de datos
    public boolean update(ContentValues values){
        SQLiteDatabase db = bbdd.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + table + " WHERE " + id + " = ?", new String[] {values.getAsString(id)});

        if(c != null){
            ContentValues valAA = new ContentValues();
            if(c.moveToNext()){
                for(String s: values.keySet()){

                    Integer column = c.getColumnIndex(s);

                    String value = c.getString(column);

                    if(values.get(s) instanceof Integer){
                        if(Integer.parseInt(value) != (Integer) values.get(s)){
                            valAA.put(s, (Integer) values.get(s));
                        }
                    }
                    else{
                        if(values.get(s) instanceof Double){
                            if(Double.parseDouble(value) != (Double) values.get(s)){
                                valAA.put(s, (Double) values.get(s));
                            }
                        }
                        else {
                            if(values.get(s) instanceof Boolean){
                                if(Boolean.parseBoolean(value) != (boolean) values.get(s)) {
                                    valAA.put(s, (Boolean) values.get(s));
                                }
                            }
                            else {
                                if (!value.trim().equals(values.get(s).toString().trim())) {
                                    valAA.put(s, values.get(s).toString());
                                }
                            }
                        }
                    }
                }
            }

            if(valAA.size() > 0){
                db = bbdd.getWritableDatabase();
                db.update(table, valAA, id + " = ?", new String[] {values.get(id) + ""});
                return true;
            }
        }
        return false;
    }
}
