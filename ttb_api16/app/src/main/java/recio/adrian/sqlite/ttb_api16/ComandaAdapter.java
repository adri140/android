package recio.adrian.sqlite.ttb_api16;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import recio.adrian.sqlite.ttb_api16.ttb.Comanda;

public class ComandaAdapter extends ArrayAdapter {

    private Activity context;
    private Comanda[] coms;

    public ComandaAdapter(Activity context, Comanda[] coms){
        super(context, R.layout.comanda_list, coms);
        this.coms = coms;
        this.context = context;
    }

    @Override
    public View getView(int position,View convertView, ViewGroup parent) {

        LayoutInflater l_inflater = context.getLayoutInflater();

        View item = l_inflater.inflate(R.layout.comanda_list, null);

        TextView t_totalPrice = (TextView) item.findViewById(R.id.priceComanda);
        TextView t_idComanda = (TextView) item.findViewById(R.id.idComanda);
        TextView t_fchEntrega = (TextView) item.findViewById(R.id.fchEntrega);

        if(this.coms != null) {
            t_totalPrice.setText(coms[position].getPorts() + "");
            t_idComanda.setText(coms[position].getId() + "");

            //transforma la fecha a string con formato yyyy-mm-dd
            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
            String newDateStr = curFormater.format(coms[position].getData_Limit());

            t_fchEntrega.setText(newDateStr);
        }

        return (item);
    }
}
