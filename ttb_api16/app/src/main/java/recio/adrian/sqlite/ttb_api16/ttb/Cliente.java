package recio.adrian.sqlite.ttb_api16.ttb;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import java.io.Serializable;

import recio.adrian.sqlite.ttb_api16.DataBase.Dao;

public class Cliente implements Serializable {

    private String cif;
    private String nom;
    private String password;
    private boolean estat;

    private Cliente(String nom, String cif, String password, boolean estat){
        this.nom = nom;
        this.password = password;
        this.cif = cif;
        this.estat = estat;
    }

    //crea el cliente y lo inserta en la bbdd
    private Cliente(String nom, String cif, String password){
        this.nom = nom;
        this.cif = cif;
        this.password = password;
        this.estat = true;
    }

    public String getCif(){
        return this.cif;
    }

    public String getNom(){
        return this.nom;
    }

    public String getPassword(){
        return this.password;
    }

    public boolean isEstat(){
        return this.estat;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public void setEstat(boolean estat){
        this.estat = estat;
    }

    //recupera un objeto cliente de la base de datos, recibe el nombre de usuario y el context de la aplicación, es decir this
    public static final Cliente recoverCliente(String name, Context con){

        Dao dao = new Dao(con, "Cliente", "nom");

        ContentValues[] allValues = dao.getValues(name);

        Cliente[] clis = getCliente(allValues);

        if(clis == null) return null;

        return clis[0];
    }

    //permite crear un cliente, y lo inserta en la bbdd, por eso el context
    public static final Cliente createCliente(String nom, String cif, String password, Context context){
        Cliente er;
        try {
            er = new Cliente(nom, cif, password);

            //añade en la base de datos con un insert
            Dao c_dao = new Dao(context, "Cliente", "nom");//buscamos el cliente por nombre, el qual es unique
            c_dao.insertObject(er.getValues());
        }
        catch(Exception e){
            Log.i("MyUserError", e.getMessage());
            er = null;
        }
        return er;
    }

    @Override
    public String toString(){
        return "Nombre: " + this.getNom() + " password: " + this.getPassword() + " estat: " + this.isEstat() + " cif: " + this.getCif();
    }

    //inserta el contenido de este objeto a un content values, para ser insertado en la base de datos
    private final ContentValues getValues(){
        ContentValues values = new ContentValues();
        values.put("cif", this.getCif());
        values.put("nom", this.getNom());
        values.put("password", this.getPassword());
        values.put("estat", this.isEstat());

        return values;
    }

    //convierte los valores de un contentValue creado por el dao a un objeto de tipo cliente
    private static final Cliente[] getCliente(ContentValues[] allValues){

        Cliente[] clientes = null;
        if(allValues != null) {
            if (allValues[0] != null) {

                clientes = new Cliente[allValues.length];

                for (int p = 0; p < allValues.length; p++) {
                    clientes[p] = new Cliente((String) allValues[p].get("nom"), (String) allValues[p].get("cif"), (String) allValues[p].get("password"), Integer.parseInt((String) allValues[p].get("estat")) > 0);
                }
            } else {
                Log.i("MyClientError", "No exite ningun cliente.");
            }
        }
        else{
            Log.i("MyClientError", "No exite ningun cliente.");
        }
        return clientes;
    }

    public boolean update(Context context){
        ContentValues values = this.getValues();
        Dao dao = new Dao(context, "Cliente", "cif");
        return dao.update(values);
    }
}
