package recio.adrian.sqlite.ttb_api16;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.Date;

import recio.adrian.sqlite.ttb_api16.ttb.Cliente;
import recio.adrian.sqlite.ttb_api16.ttb.Comanda;
import recio.adrian.sqlite.ttb_api16.ttb.Enum_Estat;

public class ComandaActivity extends Activity implements AdapterView.OnItemClickListener, View.OnClickListener {

    private Cliente cli;
    private Comanda[] l_comandas;
    private ComandaAdapter c_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comanda_principal);

        Button b_return = findViewById(R.id.botReturn);
        Button b_exit = findViewById(R.id.botSession);
        Button b_addComanda = findViewById(R.id.botAdd);

        b_return.setOnClickListener(this);
        b_exit.setOnClickListener(this);
        b_addComanda.setOnClickListener(this);

        //recogemos el cliente con el que estamos trabajando
        Intent intent = getIntent();
        cli = (Cliente) intent.getSerializableExtra("CLIENTE");

        if(cli != null) {

            //hay que extraer todas las comandas
            ListView l_listComandas = findViewById(R.id.comandaList);
            l_listComandas.setOnItemClickListener(this);
            this.createAdapter();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //implementar, al hacer clic en una comanda, habres el activity con el formulario para crear una comanda, pero en vez de crear-la te permite ver esta, y modificar-la
        Intent intent = new Intent(this, ComandaViewActivity.class);
        intent.putExtra("CLIENTE", this.cli);
        intent.putExtra("COMANDA", l_comandas[position]);
        startActivityForResult(intent, 1, null);
    }

    @Override
    public void onClick(View v) {

        if(v instanceof Button){
            Button bot = (Button) v;

            System.out.println("OnClick Event");
            switch(bot.getId()){
                case R.id.botAdd:
                    //te permite crear una nueva comanda
                    Intent intent = new Intent(this, ComandaViewActivity.class);
                    intent.putExtra("CLIENTE", this.cli);
                    startActivityForResult(intent, 1, null);
                    break;
                case R.id.botReturn:
                    //vuelves atras
                    this.setResult(RESULT_OK);
                    this.finish();
                    break;
                case R.id.botSession:
                    //cierra la aplicacion
                    Intent data = new Intent();
                    data.putExtra("EXIT", 1);
                    this.setResult(RESULT_OK, data);
                    this.finish();
                    break;
                default:
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null){//si se a pulsado a return data sera null, si se a pulsado a cerrar session data no sera nullo.
            Integer exit = data.getIntExtra("EXIT", 0);
            if(exit == 1){
                this.setResult(RESULT_OK, data);
                this.finish();
            }
        }
        else{
            this.createAdapter();
        }
    }

    private void createAdapter(){
        this.l_comandas = Comanda.recoverComandas(this.cli, this);

        if(this.l_comandas != null && this.l_comandas.length > 0) {
            c_adapter = new ComandaAdapter(this, l_comandas);
            ListView l_listComandas = findViewById(R.id.comandaList);
            l_listComandas.setAdapter(c_adapter);
        }
    }
}
