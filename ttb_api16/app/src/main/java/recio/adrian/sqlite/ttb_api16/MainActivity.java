package recio.adrian.sqlite.ttb_api16;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import recio.adrian.sqlite.ttb_api16.DataBase.Data;
import recio.adrian.sqlite.ttb_api16.ttb.Cliente;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_principal);

        Data.insertProducte(this);

        Button b_atras = findViewById(R.id.botSalir);
        Button b_registro = findViewById(R.id.botRegistro);
        Button b_configUser = findViewById(R.id.botClientes);
        Button b_configProve = findViewById(R.id.botProveedores);

        b_atras.setOnClickListener(this);
        b_registro.setOnClickListener(this);
        b_configUser.setOnClickListener(this);
        b_configProve.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v instanceof Button){
            Button b_tmp = (Button) v;

            Intent intent;

            switch(b_tmp.getId()){
                case R.id.botSalir:
                    //cierra la aplicacion
                    Intent data = new Intent();
                    data.putExtra("EXIT", 1);
                    this.setResult(RESULT_OK, data);
                    this.finish();
                    break;

                case R.id.botRegistro:
                    //te envia a la configuracion del cliente
                    intent = new Intent(this, RegistroActivity.class);
                    startActivityForResult(intent, 1, null);
                    break;

                case R.id.botClientes:
                    intent = new Intent(this, LoggedUserActivity.class);
                    startActivityForResult(intent, 1, null);
                    break;
                case R.id.botProveedores:
                    //te envia a la configuracion del proveedores
                    intent = new Intent(this, ProveedoresActivity.class);
                    startActivityForResult(intent, 1, null);
                    break;

                default:
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null){//si se a pulsado a return data sera null, si se a pulsado a cerrar session data no sera nullo.
            Integer exit = data.getIntExtra("EXIT", 0);
            if(exit == 1){
                Toast.makeText(this, "Session cerrada.", Toast.LENGTH_LONG).show();
            }
        }
    }
}
