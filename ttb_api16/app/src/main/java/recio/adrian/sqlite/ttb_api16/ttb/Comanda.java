package recio.adrian.sqlite.ttb_api16.ttb;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import recio.adrian.sqlite.ttb_api16.DataBase.Dao;

public class Comanda implements Serializable {

    private Integer id = 0;
    private Date data_peticio;
    private Date data_limit;
    private Enum_Estat estat;
    private double ports;
    private String cliente;
    private Map<Producte, Integer> productos; //producto,

    private Comanda(Date data_limit, Enum_Estat estat, double ports, Cliente cli){
        this.data_peticio = new Date();
        this.data_limit = data_limit;
        this.estat = estat;
        this.ports = ports;
        this.cliente = cli.getCif();
    }

    private Comanda(Integer id, Date data_peticio, Date data_limit, Enum_Estat estat, double ports, String cliente){
        this.id = id;
        this.data_peticio = data_peticio;
        this.data_limit = data_limit;
        this.estat = estat;
        this.ports = ports;
        this.cliente = cliente;
    }

    private void setId(Integer id){
        this.id = id;
    }

    public Integer getId(){
        return this.id;
    }

    public Date getData_peticio(){
        return this.data_peticio;
    }

    public Date getData_Limit(){
        return this.data_limit;
    }

    public Enum_Estat getEstat(){
        return this.estat;
    }

    public double getPorts(){
        return this.ports;
    }

    public String getCliente(){
        return this.cliente;
    }

    public Map<Producte, Integer> getProductos(){
        return this.productos;
    }

    //estos tres estaban pensados para la parte de modificar comanda, pero no ha hávido tiempo
    public void setData_limit(Date data_limit){
        this.data_limit = data_limit;
    }

    public void setEstat(Enum_Estat estat){
        this.estat = estat;
    }

    public void setPorts(double ports){
        this.ports = ports;
    }

    public void setProductos(Map<Producte, Integer> prods){
        this.productos = prods;
    }

    public static final Comanda createComanda(Date data_limit, double ports, Cliente cli, Context context){

        Comanda com = new Comanda(data_limit, Enum_Estat.PENDIENTE, ports, cli);
        ContentValues values = com.getValues();
        Dao dao = new Dao(context, "Comanda", "id");
        com.setId(dao.insertObject(values)); //recogemos el id de la comanda

        return com;
    }

    public static final Comanda[] recoverComandas(Cliente cli, Context context){

        Dao dao = new Dao(context, "Comanda", "cliente");
        ContentValues[] values = dao.getValues(cli.getCif());
        Comanda[] coms = getComandas(values);

        if(coms == null) return null;
        else{
            for(Comanda com: coms){
                com.setProductos(Producte.recoverProducte(context, com));
            }
        }

        return coms;
    }

    private final ContentValues getValues(){
        ContentValues values = new ContentValues();

        if(id > 0) values.put("id", this.getId());

        //formatamos la fecha de pedido
        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
        String newDateStr = curFormater.format(this.getData_peticio());
        values.put("data_peticio", newDateStr);

        //formatamos la fecha de entrega
        newDateStr = curFormater.format(this.getData_Limit());
        values.put("data_limit", newDateStr);

        values.put("estat", this.getEstat().toString());
        values.put("ports", this.getPorts());
        values.put("cliente", this.getCliente());

        return values;
    }

    private static final Comanda[] getComandas(ContentValues[] allValues){
        Comanda[] comandas = null;
        if(allValues != null) {
            if (allValues[0] != null) {

                comandas = new Comanda[allValues.length];

                for (int p = 0; p < allValues.length; p++) {

                    String fech = (String) allValues[p].get("data_peticio");

                    String[] date = fech.split("-");

                    Integer year = Integer.parseInt(date[0].trim());
                    year = year - 1900; //el puto date le suma 1900 si lo introduces como int, comprovado en la alluda de android
                    Integer month = Integer.parseInt(date[1].trim());
                    month = month - 1; //esto lo ago porque al añadirlo le suma uno, no comprovado en la alluda de android
                    Integer day = Integer.parseInt(date[2].trim());

                    Date peticio =  new Date(year, month, day);

                    fech = (String) allValues[p].get("data_limit");

                    date = fech.split("-");

                    year = Integer.parseInt(date[0].trim());
                    year = year - 1900; //el puto date le suma 1900 si lo introduces como int, comprovado en la alluda de android
                    month = Integer.parseInt(date[1].trim());
                    month = month - 1; //esto lo ago porque al añadirlo le suma uno, no comprovado en la alluda de android
                    day = Integer.parseInt(date[2].trim());

                    Date limit = new Date(year, month, day);

                    comandas[p] = new Comanda(Integer.parseInt((String) allValues[p].get("id")), peticio, limit, Enum_Estat.valueOf((String) allValues[p].get("estat")), Double.parseDouble((String) allValues[p].get("ports")), (String) allValues[p].get("cliente"));
                }
            } else {
                Log.i("MyClientError", "No exite ninguna comanda.");
            }
        }
        else{
            Log.i("MyClientError", "No exite ninguna comanda.");
        }
        return comandas;
    }

    @Override
    public String toString(){
        return "id comanda: " + this.getId() + " importe: " + this.getPorts() + " fecha_limite: " + this.getData_Limit() + " fecha_pedido: " + this.getData_peticio();
    }

     //añade un producto a esta comanda, probado!!!!!
    public void addProducte(Producte prod, Integer cantidad, Context context){
       Dao db = new Dao(context, "Comanda_Producte", "idComanda");

       ContentValues values = new ContentValues();
       values.put("idComanda", this.getId());
       values.put("idProducte", prod.getId());
       values.put("cantidad", cantidad);

       db.insertObject(values);

       this.ports = this.ports + (prod.getPreu() * cantidad);

       db = new Dao(context, "Comanda", "id");

       db.update(this.getValues());
    }
}
